/*
 Navicat Premium Data Transfer

 Source Server         : 闲鱼bug修复
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : covid19

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 05/10/2021 15:48:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_isolation_central
-- ----------------------------
DROP TABLE IF EXISTS `tb_isolation_central`;
CREATE TABLE `tb_isolation_central`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `resident_id` int(0) NULL DEFAULT NULL COMMENT '居民ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_isolation_central
-- ----------------------------

-- ----------------------------
-- Table structure for tb_isolation_home
-- ----------------------------
DROP TABLE IF EXISTS `tb_isolation_home`;
CREATE TABLE `tb_isolation_home`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `resident_id` int(0) NULL DEFAULT NULL COMMENT '居民ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_isolation_home
-- ----------------------------
INSERT INTO `tb_isolation_home` VALUES (1, '2021-09-28 19:01:07', NULL, 27);
INSERT INTO `tb_isolation_home` VALUES (27, NULL, '2021-09-28 19:08:46', NULL);

-- ----------------------------
-- Table structure for tb_resident_info
-- ----------------------------
DROP TABLE IF EXISTS `tb_resident_info`;
CREATE TABLE `tb_resident_info`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `gender` int(0) NULL DEFAULT NULL COMMENT '性别（1男，0女）',
  `age` int(0) NULL DEFAULT NULL COMMENT '年龄',
  `id_number` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `tel` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详细现居住地',
  `region_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '行政区',
  `town_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '镇（街道）',
  `type` int(0) NULL DEFAULT NULL COMMENT '0:绿码，1：黄码，2：红码 3：未知',
  `status` int(0) NULL DEFAULT NULL COMMENT '0：待处理 1：正常 2：待隔离 3：居家隔离中  4:解除居家隔离5：集中隔离中 6：解除集中隔离 7：信息修正',
  `review_info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '街道反馈待确认信息',
  `back_date` date NULL DEFAULT NULL COMMENT '归淮日期（yyyy-MM-dd）',
  `relation_province` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联省',
  `relation_city` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联市',
  `relation_region` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联区',
  `relation_town` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联镇',
  `relation_community` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联社区',
  `is_central_isolation` int(0) NULL DEFAULT NULL COMMENT '是否集中隔离（0否，1是）',
  `is_home_isolation` int(0) NULL DEFAULT NULL COMMENT '是否居家隔离（0否，1是）',
  `is_nuclein_check` int(0) NULL DEFAULT NULL COMMENT '是否核酸检测（0否，1是）',
  `nuclein_check_time` datetime(0) NULL DEFAULT NULL COMMENT '核酸检测时间',
  `nuclein_result` int(0) NULL DEFAULT NULL COMMENT '核酸检测结果(0阴性，1阳性)',
  `key_area_info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '在重点地区情况',
  `town_leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '镇（街道）干部姓名',
  `town_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '镇（街道）干部电话',
  `community_leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '村（社区）干部姓名',
  `community_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '村（社区）干部电话',
  `police_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '民警姓名',
  `police_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '民警电话',
  `doctor_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医务工作者姓名',
  `doctor_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医务工作者电话',
  `grid_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '网格员姓名',
  `grid_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '网格员电话',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `is_town_push` int(0) NULL DEFAULT NULL COMMENT '是否推送镇（街道）（0否，1是）',
  `is_isolation_push` int(0) NULL DEFAULT NULL COMMENT '是否推送隔离点（0否，1是）',
  `isolation_id` int(0) NULL DEFAULT NULL COMMENT '隔离点ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_resident_info
-- ----------------------------
INSERT INTO `tb_resident_info` VALUES (26, 'candy', 0, 18, '321081200007291111', '14725836977', '102', '清江浦区', '清浦街道1', 0, 1, NULL, '2021-09-16', '江苏省', '扬州市', '仪征市', '真州镇', '幸福社区', NULL, NULL, NULL, '2021-09-21 16:00:00', 1, '不在', '补补', '147258', '补补', '147258', '补补', '147258', '补补', '147258', '补补', '147258', '册', 1, 1, 6);
INSERT INTO `tb_resident_info` VALUES (27, 'orange', 1, 18, '321081200007292222', '14725836997', '407', '淮阴区', '王家营街道', 2, 0, NULL, '2021-09-23', '江苏省', '淮安市', '淮阴区', '王家营街道', '小康社区', NULL, NULL, NULL, '2021-09-23 16:00:00', 0, '不在', '德德', '258369', '德德', '258369', '德德', '258369', '德德', '258369', '德德', '258369', '册2', 1, 1, 6);
INSERT INTO `tb_resident_info` VALUES (28, '尼古拉斯4', 1, 25, '321081200007293333', '15150843789', '315', '淮阴区', '长江路街道', 1, 2, NULL, '2021-09-23', '', '', '', '', '', NULL, NULL, NULL, '2021-09-24 00:18:51', 2, '', '', '', '', '', '', '', '', '', '', '', '', 1, 1, NULL);
INSERT INTO `tb_resident_info` VALUES (29, 'Apple', 0, 18, '321081200007295555', '14725836999', '315', '清江浦区', '清浦街道1', 2, 1, NULL, '2021-09-14', '', '', '', '', '', NULL, NULL, NULL, '2021-09-24 01:23:31', 0, '', '', '', '', '', '', '', '', '', '', '', '', 1, 0, NULL);
INSERT INTO `tb_resident_info` VALUES (30, '王昭君', 0, 18, '321081200007294444', '15150843456', '220', '淮阴区', '王家营街道', 0, 1, NULL, '2021-09-23', '', '', '', '', '', NULL, NULL, NULL, '2021-09-23 16:00:00', 1, '', '', '', '', '', '', '', '', '', '', '', '', 1, 0, NULL);
INSERT INTO `tb_resident_info` VALUES (31, '尼古拉斯', 1, 25, '321081200007296666', '15150843789', '210', '淮阴区', '王家营街道', 0, 2, NULL, '2021-09-23', '', '', '', '', '', NULL, NULL, NULL, '2021-09-23 16:00:00', 1, '', '', '', '', '', '', '', '', '', '', '', '', 1, 0, 5);
INSERT INTO `tb_resident_info` VALUES (32, '王昭君4', 0, 18, '321081200007297777', '15150843456', '102', '淮阴区', '长江路街道', 0, 2, NULL, '2021-09-06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 5);
INSERT INTO `tb_resident_info` VALUES (33, '部落冲突4', 1, 21, '321081200007298888', '15156455648', '154', '淮阴区', '长江路街道', 3, 2, NULL, '2021-09-22', '江苏省', '淮安市', '槐荫区', '王家营街道', '淮师社区', NULL, NULL, NULL, NULL, 0, '不在', '镇镇', '3870059', '村村', '3870059', '民民', '3870059', '医医', '3870059', '网网', '3870059', '注备', 1, 1, 5);
INSERT INTO `tb_resident_info` VALUES (34, '尼古拉斯4', 1, 25, '321081200007299999', '15150843789', '102', '淮阴区', '长江路街道', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 5);
INSERT INTO `tb_resident_info` VALUES (41, '12', 1, 12, '450881199704070610', '13047843941', NULL, NULL, NULL, 0, 0, NULL, '2021-10-08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-21 16:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);

-- ----------------------------
-- Table structure for tb_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `tb_role_permission`;
CREATE TABLE `tb_role_permission`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menu_id` int(0) NULL DEFAULT NULL COMMENT '菜单ID',
  `role_id` int(0) NULL DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_role_permission
-- ----------------------------
INSERT INTO `tb_role_permission` VALUES (1, 1, 1);
INSERT INTO `tb_role_permission` VALUES (2, 3, 3);
INSERT INTO `tb_role_permission` VALUES (3, 4, 3);
INSERT INTO `tb_role_permission` VALUES (4, 5, 3);
INSERT INTO `tb_role_permission` VALUES (5, 6, 4);
INSERT INTO `tb_role_permission` VALUES (6, 7, 4);
INSERT INTO `tb_role_permission` VALUES (7, 8, 4);
INSERT INTO `tb_role_permission` VALUES (8, 2, 2);
INSERT INTO `tb_role_permission` VALUES (9, 3, 2);
INSERT INTO `tb_role_permission` VALUES (10, 4, 2);
INSERT INTO `tb_role_permission` VALUES (11, 5, 2);

-- ----------------------------
-- Table structure for tb_sys_isolation
-- ----------------------------
DROP TABLE IF EXISTS `tb_sys_isolation`;
CREATE TABLE `tb_sys_isolation`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `isolation_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '隔离点名称',
  `region_id` int(0) NULL DEFAULT NULL COMMENT '行政区ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_sys_isolation
-- ----------------------------
INSERT INTO `tb_sys_isolation` VALUES (1, '第一隔离点', 1);
INSERT INTO `tb_sys_isolation` VALUES (2, '第二隔离点', 1);
INSERT INTO `tb_sys_isolation` VALUES (3, '第三隔离点', 1);
INSERT INTO `tb_sys_isolation` VALUES (4, '第四隔离点', 2);
INSERT INTO `tb_sys_isolation` VALUES (5, '第五隔离点', 2);
INSERT INTO `tb_sys_isolation` VALUES (6, '第六隔离点', 2);
INSERT INTO `tb_sys_isolation` VALUES (7, '第七隔离点', 3);
INSERT INTO `tb_sys_isolation` VALUES (8, '第八隔离点', 3);
INSERT INTO `tb_sys_isolation` VALUES (9, '第九隔离点', 4);
INSERT INTO `tb_sys_isolation` VALUES (10, '第十隔离点', 4);

-- ----------------------------
-- Table structure for tb_sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `tb_sys_menu`;
CREATE TABLE `tb_sys_menu`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单路径',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `pid` int(0) NULL DEFAULT NULL COMMENT '父节点ID',
  `level` int(0) NULL DEFAULT NULL COMMENT '菜单级别',
  `sort` int(0) NULL DEFAULT NULL COMMENT '排序',
  `hidden` int(0) NULL DEFAULT NULL COMMENT '是否隐藏',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_sys_menu
-- ----------------------------
INSERT INTO `tb_sys_menu` VALUES (1, '账号管理', 'manager/user/list', 'fa fa-users', NULL, 1, 1, 0);
INSERT INTO `tb_sys_menu` VALUES (2, '客户管理', 'region/resident/list', 'fa fa-users', NULL, 1, 2, 0);
INSERT INTO `tb_sys_menu` VALUES (3, '待入住', 'town/resident/waiting', 'fa fa-users', NULL, 1, 3, 0);
INSERT INTO `tb_sys_menu` VALUES (4, '入住中', 'town/resident/going', 'fa fa-users', NULL, 1, 4, 0);
INSERT INTO `tb_sys_menu` VALUES (5, '入住记录', 'town/resident/history', 'fa fa-users', NULL, 1, 5, 0);
INSERT INTO `tb_sys_menu` VALUES (6, '待隔离', NULL, 'fa fa-users', NULL, 1, 6, 0);
INSERT INTO `tb_sys_menu` VALUES (7, '隔离中', NULL, 'fa fa-users', NULL, 1, 7, 0);
INSERT INTO `tb_sys_menu` VALUES (8, '隔离记录', NULL, 'fa fa-users', NULL, 1, 8, 0);

-- ----------------------------
-- Table structure for tb_sys_region
-- ----------------------------
DROP TABLE IF EXISTS `tb_sys_region`;
CREATE TABLE `tb_sys_region`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `region_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '行政区名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_sys_region
-- ----------------------------
INSERT INTO `tb_sys_region` VALUES (1, '清江浦区');
INSERT INTO `tb_sys_region` VALUES (2, '淮阴区');
INSERT INTO `tb_sys_region` VALUES (3, '淮安区');
INSERT INTO `tb_sys_region` VALUES (4, '清河区');
INSERT INTO `tb_sys_region` VALUES (5, '洪泽区');

-- ----------------------------
-- Table structure for tb_sys_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_sys_role`;
CREATE TABLE `tb_sys_role`  (
  `id` int(0) NOT NULL COMMENT '主键',
  `role_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_sys_role
-- ----------------------------
INSERT INTO `tb_sys_role` VALUES (1, '超级管理员');
INSERT INTO `tb_sys_role` VALUES (2, '前台');
INSERT INTO `tb_sys_role` VALUES (3, '经理');

-- ----------------------------
-- Table structure for tb_sys_town
-- ----------------------------
DROP TABLE IF EXISTS `tb_sys_town`;
CREATE TABLE `tb_sys_town`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `town_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '街道名称',
  `region_id` int(0) NULL DEFAULT NULL COMMENT '行政区ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_sys_town
-- ----------------------------
INSERT INTO `tb_sys_town` VALUES (1, '王家营街道', 2);
INSERT INTO `tb_sys_town` VALUES (2, '长江路街道', 2);
INSERT INTO `tb_sys_town` VALUES (3, '古清河街道', 2);
INSERT INTO `tb_sys_town` VALUES (4, '清浦街道1', 1);
INSERT INTO `tb_sys_town` VALUES (5, '清浦街道2', 1);
INSERT INTO `tb_sys_town` VALUES (6, '清浦街道3', 1);
INSERT INTO `tb_sys_town` VALUES (7, '淮安街道1', 3);
INSERT INTO `tb_sys_town` VALUES (8, '淮安街道2', 3);
INSERT INTO `tb_sys_town` VALUES (9, '淮安街道3', 3);
INSERT INTO `tb_sys_town` VALUES (10, '清河街道1', 4);
INSERT INTO `tb_sys_town` VALUES (11, '清河街道2', 4);
INSERT INTO `tb_sys_town` VALUES (12, '清河街道3', 4);
INSERT INTO `tb_sys_town` VALUES (13, '洪泽街道1', 5);
INSERT INTO `tb_sys_town` VALUES (14, '洪泽街道2', 5);
INSERT INTO `tb_sys_town` VALUES (15, '洪泽街道3', 5);

-- ----------------------------
-- Table structure for tb_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_sys_user`;
CREATE TABLE `tb_sys_user`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `role_id` int(0) NULL DEFAULT NULL COMMENT '角色ID',
  `region_id` int(0) NULL DEFAULT NULL COMMENT '行政区ID',
  `town_id` int(0) NULL DEFAULT NULL COMMENT '镇(街道)ID',
  `isolation_id` int(0) NULL DEFAULT NULL COMMENT '隔离点ID',
  `realname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `tel` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_sys_user
-- ----------------------------
INSERT INTO `tb_sys_user` VALUES (1, 'admin', '	e10adc3949ba59abbe56e057f20f883e', 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_sys_user` VALUES (2, 'whs', '	e10adc3949ba59abbe56e057f20f883e', 2, 2, 1, 6, '王洪顺', '13179794925');
INSERT INTO `tb_sys_user` VALUES (3, 'zhangsan', 'e10adc3949ba59abbe56e057f20f883e', 3, 2, 1, 4, '张三', NULL);
INSERT INTO `tb_sys_user` VALUES (4, 'lisi', 'e10adc3949ba59abbe56e057f20f883e', 4, 3, NULL, 7, NULL, NULL);
INSERT INTO `tb_sys_user` VALUES (10, 'whs3', 'e10adc3949ba59abbe56e057f20f883e', 2, 2, 1, 5, '王洪顺', '13179894952');
INSERT INTO `tb_sys_user` VALUES (11, '1qu', 'e10adc3949ba59abbe56e057f20f883e', 2, 1, 4, 1, '一区', '13179894952');

SET FOREIGN_KEY_CHECKS = 1;
