package com.hs.web.town;

import com.github.pagehelper.PageInfo;
import com.hs.model.TbResidentInfo;
import com.hs.model.TbSysTown;
import com.hs.model.TbSysUser;
import com.hs.service.ResidentService;
import com.hs.service.UserService;
import com.hs.utils.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.hs.common.Constants;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/town")
public class TownController {

    @Autowired
    private UserService userService;

    @Autowired
    private ResidentService residentService;

    /**
     * 跳转到待隔离居民列表页面
     * @return
     */
    @RequestMapping("/resident/waiting")
    public String toResidentWaiting(Model model){
        TbSysUser user=(TbSysUser) SessionUtil.getPrimaryPrincipal();
        List<TbSysTown> townListAll=userService.getTownByRegionId(user.getRegionId());
        model.addAttribute("townListAll",townListAll);
        return "town/resident-list-waiting";
    }
    /**
     * 跳转到隔离中居民列表页面
     * @return
     */
    @RequestMapping("/resident/going")
    public String toResidentGoing(Model model){
        TbSysUser user=(TbSysUser) SessionUtil.getPrimaryPrincipal();
        List<TbSysTown> townListAll=userService.getTownByRegionId(user.getRegionId());
        model.addAttribute("townListAll",townListAll);
        return "town/resident-list-going";
    }

    /**
     * 跳转到隔离历史记录居民列表页面
     * @return
     */
    @RequestMapping("/resident/history")
    public String toResidentHistory(Model model){
        TbSysUser user=(TbSysUser) SessionUtil.getPrimaryPrincipal();
        List<TbSysTown> townListAll=userService.getTownByRegionId(user.getRegionId());
        model.addAttribute("townListAll",townListAll);
        return "town/resident-list-history";
    }
    /**
     * 居民列表数据查询
     * @param name
     * @param idNumber
     * @param townName
     * @param page
     * @param limit
     * @return
     */
    @RequestMapping("/resident/waiting/data.json")
    @ResponseBody
    public Map getResidentWaitingData(String name, String idNumber, String townName, Integer page, Integer limit){
        Map map=new HashMap();
        //获取登录人所属区ID
        TbSysUser user= (TbSysUser)SessionUtil.getPrimaryPrincipal();
        //查询该区居民列表
        List<TbResidentInfo> list=residentService.getResidentList(user.getRegionId(),user.getTownId(),name,idNumber,townName,Constants.RESIDENT_STATUS_0,page,limit);
        PageInfo<TbResidentInfo> pageInfo=new PageInfo<TbResidentInfo>(list);
        //封装返回接口
        map.put("code","0");
        map.put("msg","");
        map.put("count",pageInfo.getTotal());
        map.put("data",pageInfo.getList());
        return map;
    }
    /**
     * 居民列表数据查询
     * @param name
     * @param idNumber
     * @param townName
     * @param page
     * @param limit
     * @return
     */
    @RequestMapping("/resident/going/data.json")
    @ResponseBody
    public Map getResidentGoingData(String name, String idNumber, String townName, Integer page, Integer limit){
        Map map=new HashMap();
        //获取登录人所属区ID
        TbSysUser user= (TbSysUser)SessionUtil.getPrimaryPrincipal();
        //查询该区居民列表
        List<TbResidentInfo> list=residentService.getResidentList(user.getRegionId(),user.getTownId(),name,idNumber,townName,Constants.RESIDENT_STATUS_1,page,limit);
        PageInfo<TbResidentInfo> pageInfo=new PageInfo<TbResidentInfo>(list);
        //封装返回接口
        map.put("code","0");
        map.put("msg","");
        map.put("count",pageInfo.getTotal());
        map.put("data",pageInfo.getList());
        return map;
    }
    /**
     * 居民列表数据查询
     * @param name
     * @param idNumber
     * @param townName
     * @param page
     * @param limit
     * @return
     */
    @RequestMapping("/resident/history/data.json")
    @ResponseBody
    public Map getResidentHistoryData(String name, String idNumber, String townName, Integer page, Integer limit){
        Map map=new HashMap();
        //获取登录人所属区ID
        TbSysUser user= (TbSysUser)SessionUtil.getPrimaryPrincipal();
        //查询该区居民列表
        List<TbResidentInfo> list=residentService.getResidentList(user.getRegionId(),user.getTownId(),name,idNumber,townName,Constants.RESIDENT_STATUS_2,page,limit);
        PageInfo<TbResidentInfo> pageInfo=new PageInfo<TbResidentInfo>(list);
        //封装返回接口
        map.put("code","0");
        map.put("msg","");
        map.put("count",pageInfo.getTotal());
        map.put("data",pageInfo.getList());
        return map;
    }

    /**
     * 居家隔离开始
     * @param residentId
     * @return
     */
    @RequestMapping("/isolation/home/start")
    @ResponseBody
    public String startIsolationHome(Integer residentId){
        int result=residentService.updateIsolationHomeInfoForStart(residentId,new Date());
        if(result==2){
            return "ok";
        }
        return "error";
    }

    /**
     * 居家隔离结束
     * @param residentId
     * @return
     */
    @RequestMapping("/isolation/home/end")
    @ResponseBody
    public String endIsolationHome(Integer residentId){
        int result=residentService.updateIsolationHomeInfoForEnd(residentId,new Date());
        if(result==2){
            return "ok";
        }
        return "error";
    }
}
