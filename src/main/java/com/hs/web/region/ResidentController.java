package com.hs.web.region;

import com.github.pagehelper.PageInfo;
import com.hs.model.*;
import com.hs.service.RegionService;
import com.hs.service.ResidentService;
import com.hs.service.UserService;
import com.hs.utils.ExcelUtils;
import com.hs.utils.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/region")
public class ResidentController {

    @Autowired
    private UserService userService;

    @Autowired
    private ResidentService residentService;

    @Autowired
    private RegionService regionService;
    /**
     * 跳转到居民列表页面
     * @return
     */
    @RequestMapping("/resident/list")
    public String toResident(Model model){
        TbSysUser user=(TbSysUser) SessionUtil.getPrimaryPrincipal();
        List<TbSysTown> townListAll=userService.getTownByRegionId(user.getRegionId());
        model.addAttribute("townListAll",townListAll);
        return "region/resident-list";
    }

    /**
     * 跳转到客户添加页面
     * @return
     */
    @RequestMapping("/resident/add")
    public String toResidentAdd(Model model){
        /*List<TbSysRegion> regionList=userService.getRegionAll();
        model.addAttribute("regionList",regionList);*/
        return "region/resident-add";
    }

    /**
     * 保存居民信息
     * @param resident
     * @return
     */
    @RequestMapping("/resident/save")
    @ResponseBody
    public String saveResident(TbResidentInfo resident){
        //判断该居民是否已存在
        TbResidentInfo residentInfo= residentService.getResidentByIdNumber(resident.getIdNumber());
        if(residentInfo!=null){
            return "exist";
        }else{
            //设置默认状态
            /*resident.setStatus(0);*/
            resident.setIsTownPush(0);
            resident.setIsIsolationPush(0);
            int result=residentService.saveResident(resident);
            if(result==1){
                return "ok";
            }
            return "error";
        }
    }

    /**
     * 客户列表数据查询
     * @param name
     * @param idNumber
     * @param townName
     * @param page
     * @param limit
     * @return
     */
    @RequestMapping("/resident/list/data.json")
    @ResponseBody
    public Map getResidentListData(String name,String idNumber,String townName,Integer page,Integer limit){
        Map map=new HashMap();
        //查询该区客户列表
        List<TbResidentInfo> list=residentService.getResidentList(name,idNumber,townName,page,limit);
        PageInfo<TbResidentInfo> pageInfo=new PageInfo<TbResidentInfo>(list);
        //封装返回接口
        map.put("code","0");
        map.put("msg","");
        map.put("count",pageInfo.getTotal());
        map.put("data",pageInfo.getList());
        return map;
    }

    /**
     * 导出居民信息表模板
     * @param response
     * @param session
     */
    @RequestMapping("/resident/template")
    public void printExcelTemplate(HttpServletResponse response, HttpSession session){
        //查询示例数据
        TbSysUser user=(TbSysUser) SessionUtil.getPrimaryPrincipal();
        List<TbResidentInfo> list=residentService.getResidentList(user.getRegionId());
        try{
            ExcelUtils.printEasyExcelTemplate(list,"residentInfo-template",response,session);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 批量导入居民信息
     * @param file
     * @return
     */
    @RequestMapping("/resident/import")
    @ResponseBody
    public Map importResidentExcelData(MultipartFile file){
        int code=0;
        String msg="";
        int result=0;
        try{
            //解析Excel文件
            List<TbResidentInfo> list=ExcelUtils.importExcel(file,TbResidentInfo.class);
            int size=list.size();
            //判断居民是否已存在
            list=residentService.screenResidents(list);
            //获取当前登录人的所属行政区
            TbSysUser user=(TbSysUser)SessionUtil.getPrimaryPrincipal();
            TbSysRegion region=regionService.getRegionById(user.getRegionId());
            result=residentService.saveResidentBatch(list,region.getRegionName());
            msg="成功导入"+result+"条记录,其中"+(size-list.size())+"条记录重复";
        }catch(Exception e){
            e.printStackTrace();
            code=-1;
            msg="系统异常，请联系管理员";
        }finally {
            Map map=new HashMap();
            map.put("code",code);
            map.put("msg",msg);
            map.put("data",result);
            return map;
        }
    }

    /**
     * 单删用户
     * @param id
     * @return
     */
    @RequestMapping("/resident/delete")
    @ResponseBody
    public String deleteResident(Integer id){
        int result=residentService.deleteResident(id);
        if(result==1) {
            return "ok";
        }else {
            return "error";
        }
    }

    /**
     * 批量删除用户
     * @param ids
     * @return
     */
    @RequestMapping("/resident/deleteArray")
    @ResponseBody
    public String deleteArrayResident(Integer[] ids){
        int result=residentService.deleteResidents(ids);
        if(result>0) {
            return "ok";
        }else {
            return "error";
        }
    }

    /**
     * 跳转到居民编辑页面
     * @param model
     * @param idNumber
     * @return
     */
    @RequestMapping("/resident/edit/{idNumber}")
    public String editResident(Model model,@PathVariable("idNumber") String idNumber){
        //查询行政区下拉列表
       /* List<TbSysRegion> regionList=userService.getRegionAll();*/
        TbResidentInfo residentInfo=residentService.getResidentByIdNumber(idNumber);
       /* String regionName=residentInfo.getRegionName();*/
        /*Integer regionId=regionService.getRegionIdByRegionName(regionName);
        List<TbSysTown> townList=userService.getTownByRegionId(regionId);*/
        /*model.addAttribute("regionList",regionList);*/
       /* model.addAttribute("townList",townList);*/
        model.addAttribute("residentInfo",residentInfo);
        return "region/resident-edit";
    }

    /**
     * 更新居民信息
     * @return
     */
    @RequestMapping("/resident/update")
    @ResponseBody
    public String updateResident(TbResidentInfo residentInfo){
        int result=residentService.updateResident(residentInfo);
        if(result==1) {
            return "ok";
        }else {
            return "error";
        }
    }

    /**
     * 根据行政区名称查询街道列表
     * @param regionName
     * @return
     */
    @RequestMapping("/resident/list/town.json")
    @ResponseBody
    public Map getTownJson(String regionName){
        List<TbSysTown> towns=userService.getTownByRegionName(regionName);
        Map<String,Object> map=new HashMap<String, Object>();
        map.put("towns",towns);
        return map;
    }

    /**
     * 推送入住
     * @param ids
     * @return
     */
    @RequestMapping("/push/town")
    @ResponseBody
    public Map pushTown(Integer[] ids){
        Map map=new HashMap();
        //判断有哪些是正在隔离中不能推送的
        List<Integer> list=residentService.getResidentCannotPush(ids);
        Integer[] array=new Integer[ids.length-list.size()];
        int index=0;
        for(int i=0;i<ids.length;i++){
            if(list.contains(ids[i])){
                continue;
            }
            array[index]=ids[i];
            index++;

        }
        if(array.length!=0){
            int result=residentService.updateResidentForPushTown(array);
            if(result>0){
                map.put("msg","成功推送"+array.length+"条记录，"+list.size()+"人已经入住或离店无法推送");
                map.put("code","ok");
                return map;
            }
            map.put("code","error");
            return map;
        }
        map.put("msg",list.size()+"人正在隔离中无法推送");
        map.put("code","question");
        return map;
    }

    /**
     * 获取隔离点下拉菜单
      * @param model
     * @return
     */
    @RequestMapping("/isolation/select")
    public String isolationSelect(Model model){
        //根据登录人所属行政区查询辖区隔离点
        TbSysUser user=(TbSysUser)SessionUtil.getPrimaryPrincipal();
        List<TbSysIsolation> list=userService.getIsolationByRegionId(user.getRegionId());
        model.addAttribute("list",list);
        return "region/isolation-select";
    }

    /**
     * 推送给隔离点
     * @param ids
     * @param isolationId
     * @return
     */
    @RequestMapping("/push/isolation")
    @ResponseBody
    public Map pushIsolation(String ids,Integer isolationId){
        Map map=new HashMap();
        if(ids!=null&&!"".equals(ids)){
            String[] idsArray=ids.split(",");
            //用Lambda表达式将String数组转换为int数组
            int[] intIds=Arrays.stream(idsArray).mapToInt(Integer::parseInt).toArray();

            //判断有哪些是正在隔离中不能推送的
            List<Integer> list=residentService.getResidentCannotPush(intIds);
            int[] array=new int[intIds.length-list.size()];
            int index=0;
            for(int i=0;i<intIds.length;i++){
                if(list.contains(intIds[i])){
                    continue;
                }
                array[index]=intIds[i];
                index++;

            }
            if(array.length!=0){
                int result=residentService.updateResidentForPushIsolation(array,isolationId);
                if(result>0){
                    map.put("msg","成功推送"+array.length+"条记录，"+list.size()+"人正在隔离中无法推送");
                    map.put("code","ok");
                    return map;
                }
                map.put("code","error");
                return map;
            }
            map.put("msg",list.size()+"人正在隔离中无法推送");
            map.put("code","question");
            return map;
        }
        map.put("code","error");
        return map;
    }
}
