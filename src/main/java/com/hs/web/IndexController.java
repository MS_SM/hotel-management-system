package com.hs.web;

import com.hs.model.TbSysUser;
import com.hs.service.MenuService;
import com.hs.utils.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
public class IndexController {

    @Autowired
    private MenuService menuService;

    /**
     * 跳转首页
     * @return
     */
    @RequestMapping("/index")
    public String index(){
        return "index";
    }

    /**
     * 获取菜单列表
     * @return
     */
    @RequestMapping("/menu.json")
    @ResponseBody
    public Map getMenus(){
        //从shiro的session中获取角色ID
        TbSysUser user=(TbSysUser)SessionUtil.getPrimaryPrincipal();
        Map<String,Object> map=menuService.getMenuByRoleId(user.getRoleId());
        return map;
    }
}
