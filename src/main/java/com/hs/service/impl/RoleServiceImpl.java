package com.hs.service.impl;

import com.hs.mapper.TbSysRoleMapper;
import com.hs.model.TbSysRole;
import com.hs.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private TbSysRoleMapper roleMapper;

    /**
     * 查询所有角色
     * @return
     */
    @Override
    public List<TbSysRole> getRoleAll() {
        return roleMapper.selectAll();
    }
}
