package com.hs.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hs.mapper.CustomerMapper;
import com.hs.model.Customer;
import com.hs.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService{

	//注入mapper代理对象
	@Autowired
	private CustomerMapper customerMapper;
	
	/**
	 * 保存客户信息
	 */
	@Override
	public int saveCustomer(Customer customer) {
		customer.setCreatetime(new Date());
		return	customerMapper.insert(customer);
	}

	/**
	 * 根据条件查询客户列表
	 */
	@Override
	public List<Map<String, Object>> getCustomerListByKeyword(Customer customer) {
		return customerMapper.selectByExample(customer);
	}

	/**
	 * 根据主键查询客户信息
	 */
	@Override
	public Customer getCustomerById(Integer id) {
		return customerMapper.selectByPrimaryKey(id);
	}

	/**
	 * 根据主键更新客户信息
	 */
	@Override
	public int updateCustomer(Customer customer) {
		return customerMapper.updateByPrimaryKeySelective(customer);
	}

	/**
	 * 根据主键删除客户
	 */
	@Override
	public int deleteCustomer(Integer id) {
		return customerMapper.deleteByPrimaryKey(id);
	}
	
	/**
	 * 批量删除客户
	 */
	@Override
	public int deleteCustomers(String ids) {
		return customerMapper.deleteByPrimaryKeys(ids);
	}

	@Override
	public int deleteCustomers(Integer[] ids) {
		int result=0;
		if(ids.length>0) {
			result=customerMapper.deleteByArray(ids);
		}
		return result;
	}
}
