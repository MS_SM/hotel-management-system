package com.hs.service;

import com.hs.model.TbSysRole;

import java.util.List;

public interface RoleService {
    public List<TbSysRole> getRoleAll();
}
