package com.hs.service;

import java.util.List;
import java.util.Map;

import com.hs.model.Customer;

public interface CustomerService {
	
	public int saveCustomer(Customer customer);
	
	public List<Map<String, Object>> getCustomerListByKeyword(Customer customer);
	
	public Customer getCustomerById(Integer id);
	
	public int updateCustomer(Customer customer);
	
	public int deleteCustomer(Integer id);
	
	public int deleteCustomers(String ids);
	
	public int deleteCustomers(Integer[] ids);
}
