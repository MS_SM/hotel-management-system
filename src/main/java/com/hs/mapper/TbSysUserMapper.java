package com.hs.mapper;

import com.hs.model.TbSysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface TbSysUserMapper {
    int deleteByPrimaryKey(Integer id);

    int deleteByArray(@Param("ids")Integer[] ids);

    int insert(TbSysUser record);

    int insertSelective(TbSysUser record);

    TbSysUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TbSysUser record);

    int updateByPrimaryKey(TbSysUser record);

    TbSysUser selectByUsername(String username);

    List<Map> selectByKeys(@Param("username")String username, @Param("regionId")Integer regionId, @Param("townId")Integer townId, @Param("isolationId")Integer isolationId);
}