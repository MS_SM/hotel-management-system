package com.hs.mapper;

import com.hs.model.TbSysRole;

import java.util.List;

public interface TbSysRoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TbSysRole record);

    int insertSelective(TbSysRole record);

    TbSysRole selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TbSysRole record);

    int updateByPrimaryKey(TbSysRole record);

    List<TbSysRole> selectAll();
}